#include <cublas_v2.h> //cuda自带库函数
#include <cuda_runtime.h>
#include <helper_cuda.h>
#include <helper_math.h>
#include <stdio.h>
#include <device_launch_parameters.h>
#include <cusolverDn.h>

bool readFile(void *data, int lenSize, std::string filename)
{
	std::ifstream ifs(filename, std::ios::binary);
	if (!ifs) return false;
	ifs.read((char*)data, lenSize);
	ifs.close();
	return true;
}
bool writeFile(void *data, int lenSize, std::string filename)
{
	std::ofstream ofs(filename, std::ios::binary);
	if (!ofs) return false;
	ofs.write((char*)data, lenSize);
	ofs.close();
	return true;
}
void print(double *A, int m, int n, int batch = 1)
{
	for (int k = 0; k < batch; k++)
	{
		printf("batch%d:\n", k);
		for (int i = 0; i < m; i++)
		{
			for (int j = 0; j < n; j++)
			{
				printf("%.2lf  ", A[i*n + j + k * m*n]);
			}
			printf("\n");
		}
	}

}
void print(double2 *A, int m, int n, int batch = 1)
{
	for (int k = 0; k < batch; k++)
	{
		printf("batch%d:\n", k);
		for (int i = 0; i < m; i++)
		{
			for (int j = 0; j < n; j++)
			{
				printf("%.2lf+j%.2lf  ", A[i*n + j + k * m*n].x, A[i*n + j + k * m*n].y);
			}
			printf("\n");
		}
	}

}





/*
矩阵乘法：C=A*B
A:m*k
B:k*n   输入都为行为主储存
C:m*n
*/
void Matrixmulti(double *A, double *B, double *C, int m, int k, int n, int batch = 1)
{
	double *devA, *devB, *devC;
	double alpha = 1.0;
	double beta = 0.0;
	checkCudaErrors(cudaMalloc((void**)&devA, batch*m*k * sizeof(double)));
	checkCudaErrors(cudaMalloc((void**)&devB, batch*k*n * sizeof(double)));
	checkCudaErrors(cudaMalloc((void**)&devC, batch*m*n * sizeof(double)));
	checkCudaErrors(cudaMemcpy(devA, A, batch*m*k * sizeof(double), cudaMemcpyHostToDevice));
	checkCudaErrors(cudaMemcpy(devB, B, batch*k*n * sizeof(double), cudaMemcpyHostToDevice));
	checkCudaErrors(cudaMemset(devC, 0, batch*m*n * sizeof(double)));
	cublasHandle_t handle;
	cublasCreate(&handle);											 //lda     ldb           ldc
	cublasDgemm(handle, CUBLAS_OP_N, CUBLAS_OP_N, n, m, k, &alpha, devB, n, devA, k, &beta, devC, n);
	//cublasDgemmStridedBatched(handle, CUBLAS_OP_T, CUBLAS_OP_N, n, m, k, &alpha, devB, k, k*n, devA, k, m*k, &beta, devC, n, m*n, batch);
	checkCudaErrors(cudaMemcpy(C, devC, batch*m*n * sizeof(double), cudaMemcpyDeviceToHost));
	cublasDestroy(handle);

}

/*
矩阵乘法：C=A*BT  输入都为行为主储存
A:m*k
B:n*k BT:k*n B的内容按照列为主的存储顺序排列再做乘积
C:m*n
*/
void MatrixmultiTB(double *A, double *B, double *C, int m, int k, int n, int batch = 1)
{
	double *devA, *devB, *devC;
	double alpha = 1.0;
	double beta = 0.0;
	checkCudaErrors(cudaMalloc((void**)&devA, batch*m*k * sizeof(double)));
	checkCudaErrors(cudaMalloc((void**)&devB, batch*k*n * sizeof(double)));
	checkCudaErrors(cudaMalloc((void**)&devC, batch*m*n * sizeof(double)));
	checkCudaErrors(cudaMemcpy(devA, A, batch*m*k * sizeof(double), cudaMemcpyHostToDevice));
	checkCudaErrors(cudaMemcpy(devB, B, batch*k*n * sizeof(double), cudaMemcpyHostToDevice));
	checkCudaErrors(cudaMemset(devC, 0, batch*m*n * sizeof(double)));
	cublasHandle_t handle;
	cublasCreate(&handle);	

	cublasDgemm(handle, CUBLAS_OP_T, CUBLAS_OP_N, n, m, k, &alpha, devB, k, devA, k, &beta, devC, n);
	//cublasDgemmStridedBatched(handle, CUBLAS_OP_T, CUBLAS_OP_N, n, m, k, &alpha, devB, k, k*n, devA, k, m*k, &beta, devC, n, m*n, batch);
	checkCudaErrors(cudaMemcpy(C, devC, batch*m*n * sizeof(double), cudaMemcpyDeviceToHost));
	cublasDestroy(handle);

}
/*
矩阵乘法：C=AT*B    输入都为行为主存储
A:k*m
B:k*n
C:m*n
*/
void MatrixmultiTA(double *A, double *B, double *C, int m, int k, int n, int batch = 1)
{
	double *devA, *devB, *devC;
	double alpha = 1.0;
	double beta = 0.0;
	checkCudaErrors(cudaMalloc((void**)&devA, batch*m*k * sizeof(double)));
	checkCudaErrors(cudaMalloc((void**)&devB, batch*k*n * sizeof(double)));
	checkCudaErrors(cudaMalloc((void**)&devC, batch*m*n * sizeof(double)));
	checkCudaErrors(cudaMemcpy(devA, A, batch*m*k * sizeof(double), cudaMemcpyHostToDevice));
	checkCudaErrors(cudaMemcpy(devB, B, batch*k*n * sizeof(double), cudaMemcpyHostToDevice));
	checkCudaErrors(cudaMemset(devC, 0, batch*m*n * sizeof(double)));
	cublasHandle_t handle;
	cublasCreate(&handle);

	cublasDgemm(handle, CUBLAS_OP_N, CUBLAS_OP_T, n, m, k, &alpha, devB, n, devA, m, &beta, devC, n);
	//cublasDgemmStridedBatched(handle, CUBLAS_OP_T, CUBLAS_OP_N, n, m, k, &alpha, devB, k, k*n, devA, k, m*k, &beta, devC, n, m*n, batch);
	checkCudaErrors(cudaMemcpy(C, devC, batch*m*n * sizeof(double), cudaMemcpyDeviceToHost));
	cublasDestroy(handle);

}
void test(double *A, double *C, int m, int n)
{
	double *devA,  *devC;
	double alpha = 2.0;
	double beta = 0.0;
	cublasHandle_t handle;
	cublasCreate(&handle);
	checkCudaErrors(cudaMalloc((void**)&devA, m*n * sizeof(double)));

	checkCudaErrors(cudaMalloc((void**)&devC, m*n * sizeof(double)));
	checkCudaErrors(cudaMemcpy(devA, A, m*n * sizeof(double), cudaMemcpyHostToDevice));

	checkCudaErrors(cudaMemset(devC, 0, m*n * sizeof(double)));
	cublasDgeam(handle, CUBLAS_OP_N, CUBLAS_OP_N, 1, m*n, &alpha, devA, 1, &beta, nullptr, 1, devC, 1);
	checkCudaErrors(cudaMemcpy(C, devC, m*n * sizeof(double), cudaMemcpyDeviceToHost));
	cublasDestroy(handle);
}





/*计算A*B
A,B,C均已列为主存储
*/

void test2(double *A, double *B, double *C, int m, int k, int n, int batch = 1)
{
	double *devA, *devB, *devC;
	double alpha = 1.0;
	double beta = 0.0;
	checkCudaErrors(cudaMalloc((void**)&devA, batch*m*k * sizeof(double)));
	checkCudaErrors(cudaMalloc((void**)&devB, batch*k*n * sizeof(double)));
	checkCudaErrors(cudaMalloc((void**)&devC, batch*m*n * sizeof(double)));
	checkCudaErrors(cudaMemcpy(devA, A, batch*m*k * sizeof(double), cudaMemcpyHostToDevice));
	checkCudaErrors(cudaMemcpy(devB, B, batch*k*n * sizeof(double), cudaMemcpyHostToDevice));
	checkCudaErrors(cudaMemset(devC, 0, batch*m*n * sizeof(double)));
	cublasHandle_t handle;
	cublasCreate(&handle);

	cublasDgemm(handle, CUBLAS_OP_N, CUBLAS_OP_N, m, n, k, &alpha, devA, m, devB, k, &beta, devC, m);
	//cublasDgemmStridedBatched(handle, CUBLAS_OP_T, CUBLAS_OP_N, n, m, k, &alpha, devB, k, k*n, devA, k, m*k, &beta, devC, n, m*n, batch);
	checkCudaErrors(cudaMemcpy(C, devC, batch*m*n * sizeof(double), cudaMemcpyDeviceToHost));
	cublasDestroy(handle);
}
int main(void)
{
/*	double HA[] = { 1,2,
					3,6,
					9,6};*/
/*	double HA[] = {1,3,9,2,6,6};

//	double HB[] = {1,6,5,6
//				  2,1,1,7};
	double HB[] = {1,2,6,1,5,1,6,7};

	int m = 3;
	int k = 2;
	int n = 4;
	double Hc[50];
	//Matrixmulti(HAa, HB, Hc, m, k, n);
	//print(Hc, m, n);
	//MatrixmultiTA(HA, HB, Hc, m, k, n);
	//print(Hc, m, n);
	//test(HA, Hc, m, n);
	test2(HA, HB, Hc,  m,  k,  n);
	print(Hc, m, n);*/
	double2 A[6 * 2];
	readFile(A,6*2*sizeof(double2),"G:\\RTKNEW\\RTK\\matlab\\test.dat");
	for (int i = 0; i < 2; i++)
	{
		for (int j = 0; j < 6; j++)
		{
			printf("%lf + j%lf  ", A[i * 6 + j].x, A[i * 6 + j].y);
		}
		printf("\n");
	}
	return 0;
}