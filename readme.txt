1、调用cuda cublas乘法库说明
	（1）cuda的api均按照matlab矩阵存储格式进行运算，即按照列为主进行存储
如果通过matlab mex调用cuda API进行计算就不用担心任何问题
	（2）c里的数据格式，即行为主序进行存储。就会出现相应的问题，参照c_CUDA乘法文件里相关乘法的调用
2、mex相关问题
	(1) 打开matlab进行mex -setup 配置VS编译器为2017 C++
	(2) 在matlab安装文件夹下	E:\Program Files\MATLAB\R2019a\toolbox\distcomp\gpu\extern\src\mex  找到
mexGPUExample.cu文件。
	(3)	E:\Program Files\MATLAB\R2019a\toolbox\distcomp\gpu\extern\src\mex\win64 路径下有关于vs和cuda的配置文件。如果你安装的matlab版本下没有对应的vs版本
建议升级matlab版本。
	(4)比如我在改路径下找到了nvcc_mscvcpp2017.xml 和nvcc_mscvcpp2017_dynamic.xml 用记事本打开可以看到
	Version="10.0"
    Language="CUDA"
这说明支持的cuda版本默认为10.0如果你的电脑里没有安装10.0的cuda版本也没有关系，只需要把这两个文件下有关cuda 10.0版本的替换成你电脑所安装的版本
注意：使用例如我电脑安装的是10.2版本。打开这两个文件10.0全文替换为10.2

注意！！！！！ 涉及软件不只是cuda，所以有可能会把别的软件版本10.0也替换成10.2了。如果出现问题需要自己修改，我们只更改cuda的版本号
注意：CUDA计算能力兼容性：CUDA 8.0以上的nvcc编译器需要删掉-gencode=arch=compute_20,code=sm_20；另外，GTX 980Ti可以添加-gencode=arch=compute_52,code=sm_52,GTX 1080Ti可以添加-gencode=arch=compute_61,code=sm_61以获得更好的性能。
具体需要自己查询你的显卡计算能力兼容性(CUDA Compute Compability)

	(5)matlab当前路径为mexGPUExample.cu路径。命令行 mexcuda -v mexGPUExample.cu 查看编译信息。mex编译成功会生成相应的.mexw64文件
x = ones(4,4,‘gpuArray’); 
y = mexGPUExample(x)
进行测试，测试成功。就可以进行自己的cuda文件编写了。

3、例如打开作者写的cuda乘法程序命令行输入 mexcuda -v MutiTest.cu
编译成功之后，即可通过matlab  C=MutiTest(A,B);进行矩阵的乘法计算。


